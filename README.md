
![](https://komarev.com/ghpvc/?username=budennovsk)
<h1 align="center">Hi there, I'm <a href="https://daniilshat.ru/" target="_blank">Sergey</a> 
<img src="https://github.com/blackcater/blackcater/raw/main/images/Hi.gif" height="32"/></h1>
<h3 align="center">I am a :factory:metrologist :triangular_ruler: engineer :hammer:, who studies backend from Russia 🇷🇺</h3>
⚡⚡⚡

[![Typing SVG](https://readme-typing-svg.herokuapp.com?color=%2336BCF7&lines=Python+Django+SQL)](https://git.io/typing-svg)
<br>
<h2 align="center">💬Учение — только свет, по народной пословице, — оно также и свобода. Ничто так не освобождает человека, как знание.
💬<p>(Иван Сергеевич Тургенев)</p></h2>
<br>
<h1>:restroom: Social: </h1>

<div id="badges">
   <a href="https://www.instagram.com/svetloewill/">
    <img src="https://img.shields.io/badge/Instagram-%23E4405F.svg?style=for-the-badge&logo=Instagram&logoColor=white" alt="Youtube Badge"/>
  </a>
</div>
<h1>:floppy_disk: Stack: </h1>

![SQLite](https://img.shields.io/badge/sqlite-%2307405e.svg?style=for-the-badge&logo=sqlite&logoColor=white) ![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white) ![Django](https://img.shields.io/badge/django-%23092E20.svg?style=for-the-badge&logo=django&logoColor=white) ![DjangoREST](https://img.shields.io/badge/DJANGO-REST-ff1709?style=for-the-badge&logo=django&logoColor=white&color=ff1709&labelColor=gray) ![jQuery](https://img.shields.io/badge/jquery-%230769AD.svg?style=for-the-badge&logo=jquery&logoColor=white) ![Vue.js](https://img.shields.io/badge/vuejs-%2335495e.svg?style=for-the-badge&logo=vuedotjs&logoColor=%234FC08D) ![PyCharm](https://img.shields.io/badge/pycharm-143?style=for-the-badge&logo=pycharm&logoColor=black&color=black&labelColor=green) ![Visual Studio Code](https://img.shields.io/badge/Visual%20Studio%20Code-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white) ![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white) ![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)

<!--
**budennovsk/budennovsk** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
<h1>:chart_with_upwards_trend: GitHub Stats: </h1>

[![trophy](https://github-profile-trophy.vercel.app/?username=budennovsk&theme=onedark)](https://github.com/ryo-ma/github-profile-trophy)

___
[![GitHub Streak](https://github-readme-streak-stats.herokuapp.com/?user=budennovsk&theme=dark)](https://git.io/streak-stats) 

___
[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=budennovsk&show_icons=true&theme=radical)](https://github.com/anuraghazra/github-readme-stats)

___
[![codewars](https://www.codewars.com/users/budennovsk/badges/large)](https://www.codewars.com/users/budennovsk/badges/large)
